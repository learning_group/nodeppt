title: nodeppt markdown 演示 speaker: 三水清 url: https://github.com/ksky521/nodeppt transition: slide3 files: /common/js/common.js,/common/css/common.css,/js/zoom.js theme: moon usemathjax: yes

[slide]

nodeppt
这可能是迄今为止最好的网页版演示库
<iframe src="http://ghbtns.com/github-btn.html?user=ksky521&repo=nodeppt&type=watch&count=true" allowtransparency="true" frameborder="0" scrolling="0" width="110" height="20" style="width:110px;height:20px; background-color: transparent;"></iframe><iframe src="http://ghbtns.com/github-btn.html?user=ksky521&repo=nodeppt&type=fork&count=true" allowtransparency="true" frameborder="0" scrolling="0" width="110" height="20" style="width:110px;height:20px; background-color: transparent;"></iframe><iframe src="http://ghbtns.com/github-btn.html?user=ksky521&repo=nodeppt&type=follow&count=false" allowtransparency="true" frameborder="0" scrolling="0" width="170" height="20" style="width:170px;height:20px; background-color: transparent;"></iframe> [slide]

为什么选择nodeppt
基于GFM的markdown语法编写 {:&.rollIn}
支持html混排，再复杂的demo也可以做！
导出网页或者pdf更容易分享
支持单页背景图片
多种模式：纵览模式，双屏模式，远程控制
可以使用画板，可以使用note做备注
支持语法高亮，自由选择highlight样式
可以单页ppt内部动效，单步动效
支持进入/退出回调，做在线demo很方便
[slide]

基本语法和样式演示
[slide]

封面样式
h1是作为封面用的，内部的都用h2
演讲者：xxx

[slide]

样式展示 {:&.flexbox.vleft}
nodeppt 让每个人都爱上做分享！

[slide]

基本语法指南
/* 先写总的配置 */
title: 这是title，网页名称
speaker: 演讲者名称
url: https://github.com/ksky521/nodeppt
transition: 全局转场动效
files: 引入的js和css文件，多个以半角逗号隔开
theme: 皮肤样式
highlightStyle: 代码高亮样式，默认monokai_sublime
usemathjax: yes 启用MathJax渲染公式

/* 以[slide] 隔开每个ppt页面 */
[slide]
## 二级标题
这里写内容即可

[slide]
...

[slide style="background-image:url('/img/bg1.png')"]

支持添加背景图片 {:&.flexbox.vleft}
使用方法：[slide style="background-image:url('/img/bg1.png')"]

完全style写法，更加灵活，视频背景、repeat背景更不在话下

[slide]

使用LaTex公式：
$$ x = {-b \pm \sqrt{b^2-4ac} \over 2a}. s = ut + \frac{1}{2}at^2 $$ 矩阵：\( x = {\begin{bmatrix} 1 & 2 & 3 \\ 4 & 5 & 6 \end{bmatrix}} \)

[slide]

支持.class/#id/自定义属性样式
使用：.class{:.class}
使用：#id{:#id}
组合使用：{:.class.class2 width="200px"}
父元素样式使用&：{:&.class}
[slide]

主页面样式
----是上下分界线
nodeppt是基于nodejs写的支持 Markdown! 语法的网页PPT

nodeppt：https://github.com/ksky521/nodeppt

[slide]

表格示例
市面上主要的css预处理器：Less\Sass\Stylus
| Less | Sass | Stylus :-------|:------:|-------:|-------- 环境 |js/nodejs | Ruby(这列右对齐) | nodejs(高亮) {:.highlight} 扩展名 | .less | .scss/.sass | .styl 特点 | 老牌，用户多，支持js解析 | 功能全，有成型框架，发展快 | 语法多样，小众 案例/框架 | Bootstrap | Compass Bootstrap Foundation Bourbon Base.Sass |

[slide]

text
.text-danger .text-success.text-primary

.text-warning.text-info.text-white.text-dark

.blue.blue2.blue3.gray.gray2.gray3

.red.red2.red3

.yellow.yellow2.yellow3.green.green2.green3

[slide]

label and link
.label.label-primary.label.label-warning.label.label-danger.label.label-default.label.label-success.label.label-info

link style mark

[slide]

blockquote
nodeppt可能是迄今为止最好用的web presentation 三水清

下面是另外一种样式

这是一个class是：pull-right的blockquote small一下 {:&.pull-right}

[slide]

buttons
.btn .btn-default .btn.btn-lg.btn-primary .btn.btn-waring .btn.btn-success .btn.btn-danger

.btn.btn-lg.btn-default .btn.btn-xs.btn-success .btn.btn-sm.btn-primary .btn.btn-rounded.btn-waring disabled.btn.btn-danger


[slide]

icons: Font Awesome

[slide]

代码格式化
使用 highlightjs 进行语法高亮
(function(window, document){
    var a = 1;
    var test = function(){
        var b = 1;
        alert(b);
    };
    //泛数组转换为数组
    function toArray(arrayLike) {
        return [].slice.call(arrayLike);
    }
}(window, document));
    
(function(window, document){
    var a = 1;
    var test = function(){
        var b = 1;
        alert(b);
    };
    //泛数组转换为数组
    function toArray(arrayLike) {
        return [].slice.call(arrayLike);
    }
}(window, document));
    
[slide data-on-enter="testScriptTag"]

支持 HTML 和 markdown 语法混编
这是html

这是css样式

将html代码直接混编到**markdown**文件中即可

我是js控制的颜色 black {:#testScriptTag}

<script> function testScriptTag(){ document.getElementById('testScriptTag').style.color = 'black'; } </script> <style> #css-demo{ color: red; } </style>
[slide]

iframe效果
<iframe data-src="http://www.baidu.com" src="about:blank;"></iframe>
[slide]

内置多套皮肤
[slide]

支持多种皮肤
color blue dark green light
[slide]

多窗口和远程控制演示
[slide]

多窗口演示
双屏演示不out！
本页面网址改成 url?_multiscreen=1，支持多屏演示哦！

跟powderpoint/keynote一样的双屏功能，带有备注信息。

[slide]

nodeppt动效和转场演示
[slide]

第一部分：介绍单页slide内动效
[slide]

[magic data-transition="earthquake"]

演示magic标签效果
    
======== ## 演示earthquake转场效果 -----
      
[/magic]
[slide] [magic data-transition="cover-circle"]

换个magic动效效果

 [/magic]

[slide]

动效：fadeIn
列表支持渐显动效哦 {:&.fadeIn}
使用方法
markdown列表第一条加上：{:&.动效类型}
动效类型
fadeIn
rollIn
bounceIn
moveIn
zoomIn
[slide]

动效：zoomIn
列表支持渐显动效哦 {:&.zoomIn}
动效类型
fadeIn
rollIn
bounceIn
moveIn
zoomIn
[slide]

动效：bounceIn
列表支持渐显动效哦 {:&.bounceIn}
动效类型
fadeIn
rollIn
bounceIn
moveIn
zoomIn
[slide]

nodeppt支持多达20多个转场动效
[slide]

20种转场动效随心换
slide/slide2/slide3
newspaper
glue
kontext/vkontext
move/circle
horizontal/horizontal3d
vertical3d
zoomin/zoomout
cards
earthquake/pulse/stick...
[slide data-transition="glue"]

这是一个glue的动效
使用方法（全局设置） 1：

transition: glue

[slide data-transition="glue"]

这是一个glue的动效
使用方法 2：

[slide data-transition="glue"]

[slide data-transition="zoomin"]

这是一个zoomin的动效
使用方法：

[slide data-transition="zoomin"]

[slide data-transition="vertical3d"]

这是一个vertical3d的动效
使用方法：

[slide data-transition="vertical3d"]

[slide]

nodeppt快捷键介绍
[slide]

快速翻页
输入页码，然后enter
使用O键，开启纵览模式，然后翻页
[slide]

动效样式强调
这段话里面的加粗和em字体会动效哦~

按下【H】键查看效果

[slide]

支持zoom.js
增加了zoom.js的支持，在演示过程中使用alt+鼠标点击，则点击的地方就开始放大，再次alt+click则回复原状

[slide]

图片，点击全屏
小萝莉

[slide]

图片，禁止全屏


[slide] [note] ##这里是note

使用n键，才能显示 [/note]

使用note笔记
note笔记是多窗口，或者自己做一些笔记用的
按下键盘【N】键测试下note，

markdown语法如下：

[note]
这里是note，{ 要换成中括号啊！！
{/note]
[slide]

使用画笔
使用画笔做标记哦~你也可以随便作画啊！
按下键盘【P】键：按下鼠标左键，在此处乱花下看看效果。

按下键盘【B/Y/R/G/M】：更换颜色，按下【1~4】：更换粗细

按下键盘【C】键：清空画板

[slide]

宽度不够？？
按下键盘【W】键，切换到更宽的页面看效果，第二次按键返回

|less| sass | stylus :-------|:------:|-------:|-------- 环境 |js/nodejs | Ruby(这列右对齐) | nodejs(高亮) {:.highlight} 扩展名 | .less | .sass/.scss | .styl 特点 | 老牌，用户多，支持js解析 | 功能全，有成型框架，发展快 | 语法多样，小众 案例/框架 | Bootstrap | compass bourbon |

[slide]

使用overview模式
按下键盘【O】键。看下效果。

在overview模式下，方向键下一页，【enter】键进入选中页

或者按下键盘【O】键，退出overview模式

[slide]

介绍下nodeppt的函数和事件
[slide] 支持单个slide事件：build/enter/leave/keypress，事件统一在[slide] 中使用data-on-X来指定一个全局函数名

build：当触发下一步操作的时会触发，event具有stop方法
keypress：在当前页面按键触发，event具有stop方法
enter/leave：进入/离开 此页面触发的事件，event无stop方法
[slide data-on-leave="outcallback" data-on-enter="incallback" ]

使用回调
[slide data-on-leave="fnName"]
进入执行回调incallback函数
[slide data-on-enter="fnName"]
退出执行outcallback函数
亦可以组合写：

[slide data-on-leave="foo" data-on-enter="bar"]

[slide]

远程执行函数
在多屏和远程模式中，可以使用proxyFn来远程执行函数。

<script>
function globalFunc(){
}
</script>
<button onclick="Slide.proxyFn('globalFunc')" class="btn btn-default">远程执行函数</button>
测试远程执行函数 在多屏中测试远程执行

<script> function globalFunc(a){ alert('proxyFn success: '+a+location.href); } </script>
[slide]

更多玩法
https://github.com/ksky521/nodeppt

什么？这些功能还不够用？

socket远程控制可以在手机上摇一摇换页哦~

查看项目目录ppts获取更多帮助信息